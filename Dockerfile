FROM ubuntu:16.04

MAINTAINER Aram Ghazanchyan <ghazanchyan@gmail.com>

ENV LANG=C.UTF-8
ENV DEFAULT_LOCALE=en_US
ENV DEBIAN_FRONTEND=noninteractive
ENV DOCUMENT_ROOT=/app/public

ADD docker /docker

RUN apt-get update &&\
    apt-get upgrade -y &&\
    apt-get install -y software-properties-common &&\
    add-apt-repository -y ppa:ondrej/php &&\
    add-apt-repository -y ppa:nginx/stable &&\
    apt-get update &&\
    apt-get install -y \
      vim supervisor nginx curl git pkg-config \
      libmemcached-dev \
      libmemcached11 \
      libmagickwand-dev \
      php7.3-fpm \
      php7.3-cli \
      php7.3-bcmath \
      php7.3-dev \
      php7.3-common \
      php7.3-json \
      php7.3-opcache \
      php7.3-readline \
      php7.3-mbstring \
      php7.3-curl \
      php7.3-gd \
      php7.3-mysql \
      php7.3-zip \
      php7.3-pgsql \
      php7.3-intl \
      php7.3-xml \
      php7.3-tidy \
      php7.3-xmlrpc \
      php7.3-xsl \
      php7.3-mongo \
      php7.3-ldap \
      php-apcu \
      php-mongodb \
      php-redis \
      php-igbinary \
      php-pear \
      libmcrypt-dev

# Node 12 install
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs

RUN pecl -d php_suffix=7.3 install -o -f redis memcached mcrypt-1.0.3 \
    && echo "extension=redis.so" > /etc/php/7.3/mods-available/redis.ini \
    && echo "extension=memcached.so" > /etc/php/7.3/mods-available/memcached.ini \
    && echo "extension=mcrypt.so" > /etc/php/7.3/mods-available/imagick.ini \
    && ln -sf /etc/php/7.3/mods-available/redis.ini /etc/php/7.3/fpm/conf.d/20-redis.ini \
    && ln -sf /etc/php/7.3/mods-available/redis.ini /etc/php/7.3/cli/conf.d/20-redis.ini \
    && ln -sf /etc/php/7.3/mods-available/memcached.ini /etc/php/7.3/fpm/conf.d/20-memcached.ini \
    && ln -sf /etc/php/7.3/mods-available/memcached.ini /etc/php/7.3/cli/conf.d/20-memcached.ini \
    && ln -sf /etc/php/7.3/mods-available/mcrypt.ini /etc/php/7.3/fpm/conf.d/20-mcrypt.ini \
    && ln -sf /etc/php/7.3/mods-available/mcrypt.ini /etc/php/7.3/cli/conf.d/20-mcrypt.ini \
    && apt-get purge -y --auto-remove software-properties-common

RUN curl -s https://getcomposer.org/installer | php &&\
    mv composer.phar /usr/local/bin/composer

RUN rm /etc/nginx/nginx.conf &&\
    cp /docker/nginx/nginx.conf /etc/nginx/nginx.conf

RUN rm /etc/nginx/sites-enabled/* &&\
    cp /docker/nginx/nginx_vhost /etc/nginx/sites-available/ &&\
    ln -s /etc/nginx/sites-available/nginx_vhost /etc/nginx/sites-enabled/nginx_vhost

RUN cp /docker/fpm/www.conf /etc/php/7.3/fpm/pool.d/www.conf &&\
    cp /docker/supervisor/supervisord.conf /etc/supervisord.conf

RUN mkdir /app &&\
    chown -R www-data:www-data /app

RUN rm -R /docker

EXPOSE 80

ENTRYPOINT ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]